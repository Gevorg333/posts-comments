# Posts & Comments App

This is a Python Flask based application with user login ability. 

After login user can view posts, add comments and read all comments for each post.

Socket IO is used to show notification when other users are typing. 

In comments page for showing dynamically new added comments notification is used Ajax.

## Used and Required Technologies

* Python **(required >= v3.9 before project installation)**
* MySQL
* JavaScript/jQuery/Ajax
* Python Flask
* Socket IO
* Bootstrap
* CKEditor

## Download and Install Required Technologies

* [Python 3](https://www.python.org/downloads/)

## Installation

For commands use default terminal on Unix/macOS OS. On Windows OS use GitBash.

Open terminal, navigate to the destination folder where you want to clone project repository. 

Run below commands one by one.

### Clone Repository and Go Inside Project's Folder

```bash
git clone https://Gevorg333@bitbucket.org/Gevorg333/posts-comments.git
cd posts-comments/
```

### Create virtual environment.

On Windows

```bash
python -m venv venv
```

On Unix/macOS

```bash
python3 -m venv venv
```

### Activate project

On Windows

```bash
source venv/Scripts/activate
```

On Unix/macOS

```bash
source venv/bin/activate
```

### Install Required Packages

On Windows/Unix/macOS

```bash
pip install -r requirements.txt
```

## Run Application

On Windows

```bash
python app.py
```

On Unix/macOS

```bash
python3 app.py
```

After this you can open the project in browser via this link:

[http://127.0.0.1:5000/](http://127.0.0.1:5000/)

## Additional Information

### Posts Info

Created 3 posts for tests. No need to add new posts.

### Users Info

Created 3 users. Below are listed the credentials for them:

* `Username: john_doe` `Password: user`
* `Username: kate_lie` `Password: user`
* `Username: duke_wang` `Password: user`

### Remote Database Credentials

* [RemoteMySQL](https://remotemysql.com/login.php) - `Email: gevorgforandroid@gmail.com` `Password: tDiXFXLdn#pa2de`
* [RemotePHPMyAdmin](https://remotemysql.com/phpmyadmin/index.php) - `Username: KmvrxBTguf` `Password: o4fNfj0x1Y`

## Final Notes

* Have done some SQL query changes according needs and add one-to-one relationship between tables 
* No need to do changes in remote DB. I am using it to avoid headache MySQL server installation on local environment.
* The application styling part haven't done as the time was strict. 
Anyway nothing is listed related to styling in task description.
* Show new comments banner on Comments page is done via Ajax, but could be done via Socket IO too 
(just would take longer to realize).
* To see comments banner just keep one Comments page opened in one window and in another window go the parent post page 
of the comments and submit typed message then in Comments page wait 10 seconds (Ajax requests interval is set 10 
seconds) to see the banner
* Typing message note is done via Socket IO.
* To check typing message part just open link via different browsers, put them in one screen, login via different users 
there and go to the same single post page and start typing.
* SQL dump file you are able to find in `sql-dump` folder.
* For questions just contact me via email or phone. 

## The End
