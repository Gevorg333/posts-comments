from flask import Flask, render_template, flash, redirect, url_for, session, request, jsonify
from flask_mysqldb import MySQL
from datetime import datetime, timezone
from wtforms import Form, TextAreaField, validators
from passlib.hash import sha256_crypt
from functools import wraps
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)

# Socket Config
app.config['SECRET_KEY'] = 'appSecretKey'
socketio = SocketIO(app, cors_allowed_origins='*', logger=True, engineio_logger=True)

# Config MySQL
app.config['MYSQL_HOST'] = 'remotemysql.com'
app.config['MYSQL_USER'] = 'KmvrxBTguf'
app.config['MYSQL_PASSWORD'] = 'o4fNfj0x1Y'
app.config['MYSQL_DB'] = 'KmvrxBTguf'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

# init MYSQL
mysql = MySQL(app)


# Homepage
@app.route('/')
def index():
    return render_template('home.html')


# User login
@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Get Form Fields
        username = request.form['username']
        password_candidate = request.form['password']

        # Create cursor
        cur = mysql.connection.cursor()

        # Get user by username
        result = cur.execute("SELECT * FROM users WHERE username = %s", [username])

        if result > 0:
            # Get stored hash
            data = cur.fetchone()
            password = data['password']

            # Compare Passwords
            if sha256_crypt.verify(password_candidate, password):
                # Passed
                session['logged_in'] = True
                session['username'] = username
                session['firstName'] = data['first_name']
                session['lastName'] = data['last_name']
                session['user_id'] = data['id']

                flash('You are now logged in', 'success')
                return redirect(url_for('posts'))
            else:
                error = 'Invalid login'
                return render_template('login.html', error=error)

            # Close connection
            cur.close()
        else:
            error = 'Username not found'
            return render_template('login.html', error=error)

    return render_template('login.html')


# Check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))

    return wrap


# Logout
@app.route('/logout/')
@is_logged_in
def logout():
    session.clear()

    flash('You are now logged out', 'success')
    return redirect(url_for('login'))


# Posts
@app.route('/posts/')
@is_logged_in
def posts():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get posts
    result = cur.execute("SELECT * FROM posts")

    posts = cur.fetchall()

    if result > 0:
        return render_template('posts.html', posts=posts)
    else:
        msg = 'No Posts Found'
        return render_template('posts.html', msg=msg)
    
    # Close connection
    cur.close()


# Post Comment Form Class
class CommentForm(Form):
    comment_body = TextAreaField('Comment', [validators.Length(min=5)])


# Single Post
@app.route('/post/<post_id>/', methods=['GET', 'POST'])
@is_logged_in
def post(post_id):
    # Create cursor
    cur = mysql.connection.cursor()

    if request.method == 'GET':
        # Get post
        result = cur.execute("SELECT * FROM posts WHERE id = %s", [post_id])

        post = cur.fetchone()

    form = CommentForm(request.form)

    if request.method == 'POST' and form.validate():
        comment_body = form.comment_body.data

        # Execute
        cur.execute(
            "INSERT INTO comments (post_id, user_id, comment) VALUES(%s, %s, %s)",
            (post_id, session['user_id'], comment_body)
        )

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('Comment is added', 'success')

        return redirect(url_for('post', post_id=post_id))

    return render_template('post.html', post=post, form=form)


# Post Comments
@app.route('/comments/<post_id>/', methods=['GET', 'POST'])
@is_logged_in
def comments(post_id):
    # Create cursor
    cur = mysql.connection.cursor()

    if request.method == 'GET':
        # Get comments
        result_1 = cur.execute(
            "SELECT "
            "c.id commentID, c.comment userComment, c.created commentDate, "
            "u.username, u.first_name firstName, u.last_name lastName, "
            "p.title postTitle "
            "FROM comments as c "
            "LEFT JOIN users as u "
            "ON c.user_id = u.id "
            "LEFT JOIN posts as p "
            "ON c.post_id = p.id "
            "WHERE c.post_id = %s "
            "ORDER BY c.created",
            [post_id]
        )

        comments = cur.fetchall()

        result_2 = cur.execute("SELECT id, title FROM posts WHERE id = %s", [post_id])

        post = cur.fetchone()

        return render_template('comments.html', comments=comments, post=post, today=datetime.now(timezone.utc))

    if request.method == 'POST':
        ajax_response = request.form

        result_1 = cur.execute(
            "SELECT "
            "c.id commentID, c.comment userComment, c.created commentDate, "
            "u.username, u.first_name firstName, u.last_name lastName, "
            "p.title postTitle "
            "FROM comments as c "
            "LEFT JOIN users as u "
            "ON c.user_id = u.id "
            "LEFT JOIN posts as p "
            "ON c.post_id = p.id "
            "WHERE c.post_id = %s AND c.created > %s "
            "ORDER BY c.created",
            [ajax_response['current_post_id'], ajax_response['page_opened_dateTime']]
        )

        latest_comments = cur.fetchall()

        return jsonify(latest_comments)


# Socket for typing message
@socketio.on('typing_in_out')
def handle_message_typing_show(data):
    socketio.emit('display_hide_typing', data, broadcast=True)


if __name__ == '__main__':
    app.debug = True
    socketio.run(app)
