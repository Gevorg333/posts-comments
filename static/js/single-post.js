$(function () {
    const pagePathArray = window.location.pathname.split('/');
    let socket = io.connect(`http://127.0.0.1:5000`);

    const currentUserID = $('.current-user-id').val();
    const currentUserFirstName = $('.current-user-first-name').val();
    const currentPostID = $('.current-post-id').val();

    let editor = CKEDITOR.replace('editor');

    $(document).on('click', function (e) {
        if ($(e.target).is('#cke_editor') === false && $('#cke_editor').has(e.target).length === 0) {
            socket.emit(
                'typing_in_out',
                {
                    typing: false,
                    user_id: currentUserID,
                    post_id: currentPostID
                }
            )
        }
    });

    // The "change" event is fired whenever a change is made in the editor.
    editor.on('change', function (evt) {
        socket.emit(
            'typing_in_out',
            {
                typing: true,
                user_id: currentUserID,
                post_id: currentPostID,
                typing_note: `<div id="typing-row-user-${currentUserID}">${currentUserFirstName} is typing ...</div>`
            }
        )
    });

    $('#comment-form-submit-btn').on('click', function (e) {
        e.preventDefault();

        if (editor.getData() === "") {
            alert('Write Comment First!');
        } else {
            $('#add-comment-form').submit();
        }
    });

    socket.on('display_hide_typing', (data) => {
        if (
            data.typing == true &&
            $(`#typing-row-user-${data.user_id}`).length == 0 &&
            currentUserID != data.user_id &&
            data.post_id == pagePathArray[2]
        )
            $("#typing-note-container").append(data.typing_note);

        if (data.typing == false && data.post_id == pagePathArray[2])
            $(`#typing-row-user-${data.user_id}`).remove();
    })
});