$(function () {
    const pagePathArray = window.location.pathname.split('/');
    const currentPostID = pagePathArray[2];
    const requestInterval = 1000 * 10 * 1;

    let getNewComments = () => $.ajax({
        url: `/comments/${currentPostID}/`,
        method: "POST",
        data: {
            page_opened_dateTime: $('#comments-base-container').data('openedDate'),
            current_post_id: currentPostID
        },
        dataType: "json",
        success: function (result) {
            const commentsCount = result.length;

            if (commentsCount > 0) {
                let commentsCountText = 'is 1';
                let commentWord = 'comment';

                if (commentsCount > 1) {
                    commentsCountText = `are ${commentsCount}`;
                    commentWord = 'comments'
                }

                $('.new-comments-count').text(commentsCountText);
                $('.comment-word-plural-single').text(commentWord);

                $('.new-comments-banner').css('display', 'block');
            }

            setTimeout(getNewComments, requestInterval);
        }
    });

    setTimeout(getNewComments, requestInterval);

    $('.new-comment-anchor').on('click', function (e) {
        e.preventDefault();

        let firstNewCommentID = '';
        let newCommentsCount = 0;

        $.ajax({
            url: `/comments/${currentPostID}/`,
            method: "POST",
            data: {
                page_opened_dateTime: $('#comments-base-container').data('openedDate'),
                current_post_id: currentPostID
            },
            dataType: "json",
            success: function (result) {
                const commentsCount = result.length;
                newCommentsCount = commentsCount;

                if (commentsCount > 0) {
                    let firstComment = result[Object.keys(result)[0]];

                    firstNewCommentID = firstComment.commentID;

                    $.each(result, function (key, value) {
                        let commentItemDate = new Date(value.commentDate);

                        let currentMonth = (commentItemDate.getUTCMonth() + 1 < 10 ? '0' : '') + (commentItemDate.getUTCMonth() + 1);
                        let currentDay = (commentItemDate.getUTCDate() < 10 ? '0' : '') + commentItemDate.getUTCDate();
                        let currentDate = commentItemDate.getUTCFullYear() + '-' + currentMonth + '-' + currentDay;
                        let currentHours = (commentItemDate.getUTCHours() < 10 ? '0' : '') + commentItemDate.getUTCHours();
                        let currentMinutes = (commentItemDate.getUTCMinutes() < 10 ? '0' : '') + commentItemDate.getUTCMinutes();
                        let currentTime = currentHours + ":" + currentMinutes + ":" + commentItemDate.getUTCSeconds();

                        let commentItemHTML = `<div id="comment-card-${value.commentID}" class="card p-3 mt-2">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="user d-flex flex-row align-items-center">
                                        <img
                                            src="/static/images/${value.username}.jpg"
                                            width="30"
                                            class="user-img rounded-circle mr-2"
                                        >
    
                                        <span>
                                            <small class="font-weight-bold text-primary">
                                                ${value.firstName} ${value.lastName}
                                            </small>
                                        </span>
                                    </div>
                                    <small>${currentDate} ${currentTime}</small>
                                </div>
    
                                <div class="action mt-2">
                                    ${value.userComment}
                                </div>
                            </div>`;

                        $("#new-added-comments").append(commentItemHTML);
                    });
                }
            }
        }).done(function () {
            let todayObj = new Date();

            let currentMonth = (todayObj.getUTCMonth() + 1 < 10 ? '0' : '') + (todayObj.getUTCMonth() + 1);
            let currentDay = (todayObj.getUTCDate() < 10 ? '0' : '') + todayObj.getUTCDate();
            let currentHours = (todayObj.getUTCHours() < 10 ? '0' : '') + todayObj.getUTCHours();
            let currentMinutes = (todayObj.getUTCMinutes() < 10 ? '0' : '') + todayObj.getUTCMinutes();
            let currentDate = todayObj.getUTCFullYear() + '-' + currentMonth + '-' + currentDay;
            let currentTime = currentHours + ":" + currentMinutes + ":" + todayObj.getUTCSeconds();

            $('#comments-base-container').data('openedDate', `${currentDate} ${currentTime}`);
            $('.new-comments-banner').css('display', 'none');
            $('#no-comments-text-row').css('display', 'none');
            $('#all-comments-count').text(parseInt($('#all-comments-count').text())+newCommentsCount);

            $('html, body').animate({
                scrollTop: $(`#comment-card-${firstNewCommentID}`).offset().top
            }, 'slow');
        });
    });
});